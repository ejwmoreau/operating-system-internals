#include "dispatcher.h"

int main(int argc, char** argv) {

    if (argc < 2) {
        printf("Please provide a file name\n");
        return 0;
    }

    // Used to manage processes
    process* procArray[PROC_MAX];
    process* currProcess = NULL;

    // Used to read in from file
    FILE* processFile = fopen(argv[1], "r");
    char buf[BUF_MAX];
    process* pcb;

    // Initialise procArray with each given process
    for (int i = 0; fgets(buf, BUF_MAX, processFile) != NULL; i++) {
        pcb = (process*) malloc(64);

        char* arg = strtok(buf, ", ");
        pcb->arrivalTime = atoi(arg);

        arg = strtok(NULL, ", ");
        arg = strtok(NULL, ", ");
        pcb->remainingCpuTime = atoi(arg);

        procArray[i] = pcb;
    }

    int count = 0;
    int dispatcherTime = 0;
    // As long as there are still processes to run
    while (count < PROC_MAX) {
        // Decrement current process time if still running
        if (currProcess != NULL) {
            currProcess->remainingCpuTime -= 1;

            // Kill current process if no time is remaining
            if (currProcess->remainingCpuTime < 1) {
                kill(currProcess->pid, SIGINT);
                currProcess = NULL;

                // Increment count and check
                count++;
                if (count >= PROC_MAX) {
                    break;
                }
            }
        }

        // If there is no longer a running process that has arrived
        if (currProcess == NULL &&
                procArray[count] != NULL &&
                procArray[count]->arrivalTime <= dispatcherTime) {
            // Set up and run the process for one second
            currProcess = procArray[count];

            currProcess->args[0] = "./process";
            currProcess->args[1] = NULL;

            currProcess->pid = fork();

            if (currProcess->pid == 0) {
                execvp(currProcess->args[0], currProcess->args);
            }
        }

        sleep(1);

        // Increment dispatcherTime and check if running for too long
        dispatcherTime++;
        if (dispatcherTime == 21) {
            printf("Timed Out\n");
            break;
        }
    }

    // Free all process memory
    for (int i = 0; i < PROC_MAX; i++) {
        free(procArray[i]);
    }

    return 0;
}
