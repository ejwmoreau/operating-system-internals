/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2

  mab.c - memory management functions for HOST dispatcher

 *******************************************************************/

#include "hostd.h"

// Checks if size memory is available in block
bool memChk(mab* memory, int size) {
    if (memory->allocated == false &&
        memory->size >= size) {
        return true;
    }
    return false;
}

// Check if there will ever be enough memory
bool memChkMax(int size) {
    if (USER_MEMORY_SIZE < size) {
        return false;
    }
    return true;
}

// Allocates memory based on the given size
mab* memAlloc(mab* head, int size) {
    if (memChkMax(size) == false) {
        return NULL;
    }
    while (head) {
        if (memChk(head, size)) {
            head = memSplit(head, size);
            return head;
        }
        head = head->next;
    }
    return NULL;
}

// Frees the memory block and merges with nearby blocks
mab* memFree(mab* memory) {
    if (memory == NULL) {
        return NULL;
    }

    memory->allocated = false;
    while (memory->prev != NULL &&
           memory->prev->allocated == false) {
        memory = memMerge(memory->prev);
    }

    while (memory->next != NULL &&
           memory->next->allocated == false) {
        memory = memMerge(memory);
    }

    return memory;
}

// Merges the memory block with the next block
mab* memMerge(mab* memory) {
    memory->size += memory->next->size;
    memory->next = memory->next->next;
    return memory;
}

// Splits a memory block into two depending on size
mab* memSplit(mab* memory, int size) {
    if (memory->size < size) {
        return NULL;
    } else if (memory->size == size) {
        memory->allocated = true;
        return memory;
    }

    // Create and set new memory block
    mab* second = (mab*) malloc(sizeof(mab));
    second->offset = memory->offset + size;
    second->size = memory->size - size;
    second->allocated = false;
    second->next = memory->next;
    second->prev = memory;

    // Change values in old memory block
    memory->size = size;
    memory->allocated = true;
    memory->next = second;

    return memory;
}

// Prints out all memory blocks and whether they're allocated or not
void memPrint(mab* head) {
    printf("offset   size   allocated\n");
    printf("------   ----   ---------\n");
    while(head) {
        if (head->allocated == true) {
            printf("%6d %6d   allocated\n", head->offset, head->size);
        } else {
            printf("%6d %6d   free\n", head->offset, head->size);
        }
        head = head->next;
    }
}
