/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2

  hostd.c - TODO fill this

*******************************************************************/

#include "hostd.h"

int main(int argc, char** argv) {
    // Make sure there are two arguments
    if (argc != 2) {
        printf("Please supply only 2 arguments\n");
        return 1;
    }

    // Queues used to manage processes
    pcb* inputQueueHead = NULL;
    pcb* realTimeQueueHead = NULL;
    pcb* userQueueHead = NULL;
    pcb* levelQueueHeads[3] = {NULL, NULL, NULL};

    // File to save memory allocation data
    FILE* memoryFile = fopen("mem.out", "w");
    fprintf(memoryFile, "offset  size  end\n");
    fprintf(memoryFile, "------  ----  ---\n");

    // File to save process data (when allocated)
    FILE* processFile = fopen("proc.out", "w");

    // Memory block head
    mab* head = (mab*) malloc(sizeof(mab));
    head->offset = 0;
    head->size = MEMORY_SIZE;
    head->allocated = false;
    head->prev = NULL;
    head->next = NULL;

    // Allocate 64 bytes for real time processes
    head = memAlloc(head, 64);

    // Resources block for user processes
    rsrc* resources = rsrcCreate(MAX_PRINTERS,
                                 MAX_SCANNERS,
                                 MAX_MODEMS,
                                 MAX_CDS);

    // Resources block for real time processes
    rsrc* realTimeResources = rsrcCreate(0, 0, 0, 0);

    // Put all processes into inputQueue
    inputQueueHead = addAllProcesses(argv[1]);

    // Currently running process and dispatch time passed
    pcb* currProcess = NULL;
    int dispatchTime = 0;

    // While there is still a process running or to be input
    while(inputQueueHead != NULL ||
          currProcess != NULL ||
          userQueueHead != NULL) {
        printf("---------------\n");

        // Add all new processes to respective queues
        while (inputQueueHead != NULL &&
               inputQueueHead->arrivalTime <= dispatchTime) {
            pcb* process = inputQueueHead;
            inputQueueHead = deqPcb(inputQueueHead);

            // priority 0 add to realTimeQueue
            // priority 1-3 add to userQueue
            if (process->priority == 0 &&
                process->mBytes <= 64 &&
                rsrcChk(realTimeResources, process->resources) == true) {
                process->memory = head;
                realTimeQueueHead = enqPcb(realTimeQueueHead, process);
                continue;
            } else if (process->priority != 0 &&
                       memChkMax(process->mBytes) == true &&
                       rsrcChkMax(process->resources) == true) {
                userQueueHead = enqPcb(userQueueHead, process);
                continue;
            }

            // Process was rejected
            printPcb(process, processFile);
        }

        // If there are any user jobs that can be added to priority queues
        while (userQueueHead != NULL) {
            // Try and allocate memory for the process
            userQueueHead->memory = memAlloc(head, userQueueHead->mBytes);
            if (userQueueHead->memory == NULL) {
                break;
            }

            // Try and allocate resources for the process
            // If failed, then the memory will be freed
            if (rsrcAlloc(resources, userQueueHead->resources) == false) {
                userQueueHead->memory = memFree(userQueueHead->memory);
                break;
            }

            // Retrieve process
            pcb* process = userQueueHead;
            userQueueHead = deqPcb(userQueueHead);

            // Print memory allocation data to file
            fprintf(memoryFile, "%6d %5d %4d\n",
                    process->memory->offset, process->memory->size,
                    process->memory->size + process->memory->offset);

            // Put process into the appropriate queue
            if (process->priority == 1) {
                levelQueueHeads[0] = enqPcb(levelQueueHeads[0], process);
            } else if (process->priority == 2) {
                levelQueueHeads[1] = enqPcb(levelQueueHeads[1], process);
            } else {
                levelQueueHeads[2] = enqPcb(levelQueueHeads[2], process);
            }
        }

        // Decrement current process time if still running
        if (currProcess != NULL) {
            currProcess->remainingCpuTime -= 1;

            // Kill if no time remaining
            if (currProcess->remainingCpuTime < 1) {
                rsrcFree(resources, currProcess->resources);
                currProcess = killPcb(currProcess);
            // If there are no other processes waiting
            } else if (chkPcbWaiting(levelQueueHeads, realTimeQueueHead) == true &&
                       currProcess->priority != 0) {
                // If from first priority queue
                if (currProcess->priority == 1) {
                    currProcess = suspendPcb(currProcess);
                    currProcess->priority++;
                    levelQueueHeads[1] = enqPcb(levelQueueHeads[1], currProcess);
                    currProcess = NULL;
                // If from second priority queue
                } else if (currProcess->priority == 2) {
                    currProcess = suspendPcb(currProcess);
                    currProcess->priority++;
                    levelQueueHeads[2] = enqPcb(levelQueueHeads[2], currProcess);
                    currProcess = NULL;
                // If from third priority queue
                } else if (currProcess->priority == 3) {
                    currProcess = suspendPcb(currProcess);
                    levelQueueHeads[2] = enqPcb(levelQueueHeads[2], currProcess);
                    currProcess = NULL;
                }
            }

            // processes from realTimeQueue will continue running
        }

        // If there are no more processes to run at all
        if (currProcess == NULL &&
            inputQueueHead == NULL &&
            userQueueHead == NULL &&
            chkPcbWaiting(levelQueueHeads, realTimeQueueHead) == false) {
            break;
        }

        // If there is no current process and there are processes waiting
        if (currProcess == NULL &&
            chkPcbWaiting(levelQueueHeads, realTimeQueueHead) == true) {

            // Get the next process to run
            // Chosen with priority given to realTimeQueue
            if (realTimeQueueHead != NULL) {
                currProcess = realTimeQueueHead;
                realTimeQueueHead = deqPcb(realTimeQueueHead);
            } else if (levelQueueHeads[0] != NULL) {
                currProcess = levelQueueHeads[0];
                levelQueueHeads[0] = deqPcb(levelQueueHeads[0]);
            } else if (levelQueueHeads[1] != NULL) {
                currProcess = levelQueueHeads[1];
                levelQueueHeads[1] = deqPcb(levelQueueHeads[1]);
            } else if (levelQueueHeads[2] != NULL) {
                currProcess = levelQueueHeads[2];
                levelQueueHeads[2] = deqPcb(levelQueueHeads[2]);
            }

            // Either forks process or continues it
            currProcess = startPcb(currProcess, processFile);
        }

        sleep(1);

        // Increment dispatchTime
        dispatchTime++;
    }

    fclose(memoryFile);
    fclose(processFile);

    freeMemoryBlocks(head);

    return 0;
}

// Checks real time and priority queues if any processes are waiting
bool chkPcbWaiting(pcb** levelQueueHeads, pcb* realTimeQueueHead) {
    if (levelQueueHeads[0] == NULL &&
        levelQueueHeads[1] == NULL &&
        levelQueueHeads[2] == NULL &&
        realTimeQueueHead == NULL) {
        return false;
    }
    return true;
}

// Frees all memory blocks from head
void freeMemoryBlocks(mab* head) {
    while (head) {
        if (head->prev != NULL) {
            free(head->prev);
        }
        head = head->next;
    }
}

// Adds all processes from given file to inputQueue
pcb* addAllProcesses(char* filename) {

    pcb* inputQueueHead = NULL;

    // Used to read in from file
    FILE* processFile = fopen(filename, "r");
    char buf[BUF_MAX];

    // Extract all processes in file and add to input queue
    for (int i = 0; fgets(buf, BUF_MAX, processFile) != NULL; i++) {
        // Extract all process properties
        pcb* process = createNullPcb();

        char* arg = strtok(buf, ", ");
        process->arrivalTime = atoi(arg);

        arg = strtok(NULL, ", ");
        process->priority = atoi(arg);

        arg = strtok(NULL, ", ");
        process->remainingCpuTime = atoi(arg);

        arg = strtok(NULL, ", ");
        process->mBytes = atoi(arg);

        // Extract all resources for process
        arg = strtok(NULL, ", ");
        process->resources.printers = atoi(arg);

        arg = strtok(NULL, ", ");
        process->resources.scanners = atoi(arg);

        arg = strtok(NULL, ", ");
        process->resources.modems = atoi(arg);

        arg = strtok(NULL, ", ");
        process->resources.cds = atoi(arg);

        // Add process to the inputQueue
        inputQueueHead = enqPcb(inputQueueHead, process);
    }

    return inputQueueHead;
}
