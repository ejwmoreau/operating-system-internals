/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2

  rsrc.c - resource management routines for the HOST dispatcher

*******************************************************************/

#include "hostd.h"

// Create resource block with given resources
rsrc* rsrcCreate(int printers, int scanners, int modems, int cds) {
    rsrc* resources = (rsrc*) malloc(sizeof(rsrc));
    resources->printers = printers;
    resources->scanners = scanners;
    resources->modems = modems;
    resources->cds = cds;
    return resources;
}

// Checks if all required resources are available
bool rsrcChk(rsrc* available, rsrc claim) {
    if (available->printers < claim.printers) {
        return false;
    } else if (available->scanners < claim.scanners) {
        return false;
    } else if (available->modems < claim.modems) {
        return false;
    } else if (available->cds < claim.cds) {
        return false;
    }
    return true;
}

// Checks if it is ever possible to allocate required resources
bool rsrcChkMax(rsrc claim) {
    if (MAX_PRINTERS < claim.printers) {
        return false;
    } else if (MAX_SCANNERS < claim.scanners) {
        return false;
    } else if (MAX_MODEMS < claim.modems) {
        return false;
    } else if (MAX_CDS < claim.cds) {
        return false;
    }
    return true;
}

// Allocates required resources if all are available
bool rsrcAlloc(rsrc* available, rsrc claim) {
    if (rsrcChk(available, claim) == false) {
        return false;
    }

    available->printers -= claim.printers;
    available->scanners -= claim.scanners;
    available->modems -= claim.modems;
    available->cds -= claim.cds;

    return true;
}

// Frees the required resources
void rsrcFree(rsrc* available, rsrc claim) {
    available->printers += claim.printers;
    available->scanners += claim.scanners;
    available->modems += claim.modems;
    available->cds += claim.cds;
}
