#ifndef MAB_H
#define MAB_H
/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2

  mab.h - Memory Allocation definitions and prototypes for HOST dispatcher

  bool memChk(mab* memory, int size);
    - check for memory available
    - returns true or false

  bool memChkMax(int size);
    - check for over max memory
    - returns TRUE/FALSE OK/OVERSIZE

  mab* memAlloc(mab* head, int size);
    - allocate a memory block
    - returns address of block or NULL if failure

  mab* memFree(mab* memory);
    - de-allocate a memory block
    - returns address of block or merged block

  mab* memMerge(mab* memory);
    - merge memory with memory->next
    - returns memory

  mab* memSplit(mab* memory, int size);
    - split memory into two with first mab having size
    - returns memory or NULL if unable to supply size bytes

  void memPrint(mab* head);
    - print contents of all memory blocks
    - no return

 *******************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "hostd.h"

/* Memory Management *******************************/

#define MEMORY_SIZE       1024
#define RT_MEMORY_SIZE    64
#define USER_MEMORY_SIZE  (MEMORY_SIZE - RT_MEMORY_SIZE)

struct mem {
    int offset;
    int size;
    bool allocated;
    struct mem* next;
    struct mem* prev;
};

typedef struct mem mab;

/* Memory Management Function Prototypes ********/

bool memChk(mab* memory, int size);
bool memChkMax(int size);
mab* memAlloc(mab* head, int size);
mab* memFree(mab* memory);
mab* memMerge(mab* memory);
mab* memSplit(mab* memory, int size);
void memPrint(mab* head);

#endif
