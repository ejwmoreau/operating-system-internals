#ifndef PCB_H
#define PCB_H
/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2 - HOST dispatcher

  pcb - process control block functions for HOST dispatcher

  pcb* startPcb(pcb* process, FILE* processFile)
    - start (or restart) a process
    - returns pcb* of process or NULL if start (restart) failed

  pcb* suspendPcb(pcb* process)
    - suspend a process
    - returns pcb* of process

  pcb* killPcb(pcb* process)
    - kill a process
    - returns NULL if process was killed

  void printPcb(pcb* process, FILE* file)
    - print process attributes on file
    - returns pcb* of process

  pcb* createNullPcb()
    - create inactive Pcb
    - returns pcb* of newly initialised Pcb or NULL if malloc failed

  pcb* enqPcb (pcb* head, pcb* process)
    - queue process (or join queues) at end of queue
    - enqueues at tail of queue
    - returns the head of queue

  pcb* deqPcb (pcb* head);
    - dequeue process - take Pcb from head of queue
    - sets new head of queue to the next process
    - returns the new head of queue or NULL if the queue was empty

 *******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

#include "hostd.h"
#include "mab.h"
#include "rsrc.h"

/* Process Management Definitions *****************************/

#define ARGS_MAX 3

#define DEFAULT_PROCESS "./process"

#define N_QUEUES         4  /* number of queues (including RT) */
#define RT_PRIORITY      0
#define HIGH_PRIORITY    1
#define LOW_PRIORITY     (N_QUEUES - 1)
#define N_FB_QUEUES      (LOW_PRIORITY - HIGH_PRIORITY + 1)

#define PCB_UNINITIALIZED 0
#define PCB_INITIALIZED 1
#define PCB_READY 2
#define PCB_RUNNING 3
#define PCB_SUSPENDED 4
#define PCB_TERMINATED 5

struct proc {
    pid_t pid;
    char* args[ARGS_MAX];
    int arrivalTime;
    int priority;
    int remainingCpuTime;
    int mBytes;
    mab* memory;
    rsrc resources;
    int status;
    struct proc* next;
};

typedef struct proc pcb;

/* Process Management Functions *****************************/

pcb* startPcb(pcb* process, FILE* processFile);
pcb* suspendPcb(pcb* process);
pcb* killPcb(pcb* process);
void printPcb(pcb* process, FILE* file);
pcb* createNullPcb();
pcb* enqPcb(pcb* head, pcb* process);
pcb* deqPcb(pcb* head);

#endif
