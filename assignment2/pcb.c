/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2

  pcb - process control block functions for HOST dispatcher

 *******************************************************************/

#include "hostd.h"

// Start (or restart) a process
pcb* startPcb (pcb* process, FILE* processFile) {
    // If not yet started
    if (process->pid == -1) {
        // Start process
        switch (process->pid = fork ()) {
            case -1:
                perror ("startPcb");
                exit(1);
            // Child
            case 0:
                process->pid = getpid();
                process->status = PCB_RUNNING;
                execvp (process->args[0], process->args);
        }
        printPcb(process, processFile);
    // Parent: already started & suspended so continue
    } else {
        kill(process->pid, SIGCONT);
        //waitpid(process->pid, NULL, WUNTRACED);
    }
    process->status = PCB_RUNNING;
    return process;
}

// Suspend a process
pcb* suspendPcb(pcb* process) {
    kill(process->pid, SIGTSTP);
    waitpid(process->pid, NULL, WUNTRACED);
    return process;
}

// Kill a process
pcb* killPcb(pcb* process) {
    kill(process->pid, SIGINT);
    waitpid(process->pid, NULL, WUNTRACED);
    memFree(process->memory);
    free(process);
    return NULL;
}

// Print process attributes to file
void printPcb(pcb* process, FILE* file) {
    fprintf(file, "   pid | arrive | prior | cpu | offset | Mbytes | p | s | m | c | status\n");
    if (process->memory == NULL) {
        fprintf(file, "Error: Process rejected due to too much memory or resources required\n");
    } else {
    fprintf(file,
           " %5d | %6d | %5d | %3d | %6d | %6d | %d | %d | %d | %d | %6d\n",
           process->pid, process->arrivalTime, process->priority,
           process->remainingCpuTime, process->memory->offset,
           process->mBytes, process->resources.printers,
           process->resources.scanners, process->resources.modems,
           process->resources.cds, process->status);
    }
    fprintf(file, "------------------------------------------------------------------------\n");
}

// Create inactive pcb
pcb* createNullPcb() {
    pcb* process;

    if ((process = (pcb*) malloc(sizeof(pcb)))) {
        process->pid = -1;
        process->args[0] = DEFAULT_PROCESS;
        process->args[1] = NULL;
        process->arrivalTime = 0;
        process->priority = HIGH_PRIORITY;
        process->remainingCpuTime = 0;
        process->mBytes = 0;
        process->memory = NULL;
        process->resources.printers = 0;
        process->resources.scanners = 0;
        process->resources.modems = 0;
        process->resources.cds = 0;
        process->status = PCB_UNINITIALIZED;
        process->next = NULL;
        return process;
    }

    perror("allocating memory for new process");
    return NULL;
}

// Enqueue process to tail of queue
pcb* enqPcb(pcb* head, pcb* process) {
    process->next = NULL;

    if (head == NULL) {
        return process;
    }

    pcb* curr = head;
    while (curr->next != NULL) {
        curr = curr->next;
    }
    curr->next = process;
    return head;
}

// Dequeue process from head of queue
pcb* deqPcb(pcb* head) {
    pcb* process;

    if (head == NULL) {
        return NULL;
    }

    process = head;
    head = process->next;
    return head;
}
