#ifndef GHOST_H
#define GHOST_H
/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2

  hostd.h - include file for HOST dispatcher

 ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

#include "pcb.h"
#include "mab.h"
#include "rsrc.h"

#define QUANTUM  1
#define BUF_MAX 255

// Checks real time and priority queues if any processes are waiting
bool chkPcbWaiting(pcb** levelQueueHeads, pcb* realTimeQueueHead);
// Frees all memory blocks from head
void freeMemoryBlocks(mab* head);
// Adds all processes fro given file to inputQueue and returns queueHead
pcb* addAllProcesses(char* filename);

#endif
