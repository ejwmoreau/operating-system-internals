#ifndef RSRC_H
#define RSRC_H
/*******************************************************************

  Student Name: Elie Moreau
  Student SID: 430085543

  Original Author: Dr Ian G Graham, ian.graham@griffith.edu.au
  Date: December 2003

  Source: COMP3520 Tutorial Week 10 Exercise

  Note: This code is based on the original version. It has
  been edited for the purpose of completing the COMP3520 Assignment 2
  and receiving (hopefully) full marks.

 ********************************************************************

  COMP3520 Assignment 2

  rsrc.h - process resource allocation functions for HOST dispatcher

  bool rsrcChk (rsrc* available, rsrc claim)
    - check that resources are available now
    - returns true or false - no allocation is actually done

  bool rsrcChkMax (rsrc claim)
    - check that resources will ever be available
    - returns true or false

  bool rsrcAlloc (rsrc* available, rsrc claim)
    - allocate resources
    - returns true or false if not enough resources available

  void rsrcFree (rsrc* available, rsrc claim);
    - free resources
    - returns NONE

 *******************************************************************/

#include <stdbool.h>

#include "hostd.h"

/* Resource Management *****************************/

#define MAX_PRINTERS 2
#define MAX_SCANNERS 1
#define MAX_MODEMS 1
#define MAX_CDS 2

struct res {
    int printers;
    int scanners;
    int modems;
    int cds;
};

typedef struct res rsrc;

/* Prototypes  ************************************/

rsrc* rsrcCreate(int printers, int scanners, int modems, int cds);
bool rsrcChk(rsrc* available, rsrc claim);
bool rsrcChkMax(rsrc claim);
bool rsrcAlloc(rsrc* available, rsrc claim);
void rsrcFree(rsrc* available, rsrc claim);

#endif
