#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>

#define BUF_MAX 255
#define ARGS_MAX 255
#define PROC_MAX 5

struct pcb {
    pid_t pid;
    char* args[ARGS_MAX];
    int arrivalTime;
    int remainingCpuTime;
//    struct pcb* next;
};
typedef struct pcb process;

void addProcessCount(int count);
void resetProcessCount(int count);
void removeProcessCount();
int getNextProcessCount();
