#include "dispatcher.h"

int queue[PROC_MAX+1] = {-1, -1, -1, -1, -1, -1};

int main(int argc, char** argv) {

    if (argc < 2) {
        printf("Please provide a file name\n");
        return 0;
    }

    // Used to manage processes
    process* procArray[PROC_MAX];
    process* currProcess = NULL;

    // Used to read in from file
    FILE* processFile = fopen(argv[1], "r");
    char buf[BUF_MAX];
    process* pcb;

    // Initialise procArray with each given process
    for (int i = 0; fgets(buf, BUF_MAX, processFile) != NULL; i++) {
        pcb = (process*) malloc(64);
        pcb->pid = -1;

        char* arg = strtok(buf, ", ");
        pcb->arrivalTime = atoi(arg);

        arg = strtok(NULL, ", ");
        arg = strtok(NULL, ", ");
        pcb->remainingCpuTime = atoi(arg);

        procArray[i] = pcb;
    }

    int count = 0;
    int dispatcherTime = 0;
    int procCount = 0;
    // As long as there are still processes to run
    while (count < PROC_MAX) {
        // Add any processes that just arrived
        for (int i = procCount; i < PROC_MAX; i++) {
            if (procArray[i]->arrivalTime <= dispatcherTime) {
                addProcessCount(i);
                procCount++;
            } else {
                break;
            }
        }

        // Decrement current process time if still running
        if (currProcess != NULL) {
            currProcess->remainingCpuTime -= 1;

            // Kill current process and remove from queue if no time is remaining
            if (currProcess->remainingCpuTime < 1) {
                kill(currProcess->pid, SIGINT);
                removeProcessCount();
                currProcess = NULL;
            // Otherwise suspend process and reset in queue
            } else {
                kill(currProcess->pid, SIGTSTP);
                resetProcessCount(count);
                currProcess = NULL;
            }

            // Get next valid count and check
            count = getNextProcessCount();
            if (count < 0) {
                break;
            }
        }

        // If there is no longer a running process
        if (currProcess == NULL) {
            currProcess = procArray[count];
            // Set up and run the process if it hasn't been running
            if (currProcess->pid == -1) {

                currProcess->args[0] = "./process";
                currProcess->args[1] = NULL;
                /* Used for testing to avoid badly colored text
                char cpuTime[2];
                sprintf(cpuTime, "%d", currProcess->remainingCpuTime);
                currProcess->args[0] = "sleep";
                currProcess->args[1] = cpuTime;
                currProcess->args[2] = NULL;
                */

                currProcess->pid = fork();

                if (currProcess->pid == 0) {
                    execvp(currProcess->args[0], currProcess->args);
                }
            // Else continue running the process
            } else {
                kill(currProcess->pid, SIGCONT);
            }
        }

        sleep(1);
        // Used to test which process was being run
        printf("%d\n", currProcess->arrivalTime);

        // Increment dispatcherTime and check if running for too long
        dispatcherTime++;
        if (dispatcherTime == 21) {
            printf("Timed Out\n");
            break;
        }
    }

    // Free all process memory
    for (int i = 0; i < PROC_MAX; i++) {
        free(procArray[i]);
    }

    return 0;
}

// Adds given count to the queue if it's not already there
void addProcessCount(int count) {
    for (int i = 0; i < PROC_MAX; i++) {
        if (queue[i] == count) {
            return;
        } else if (queue[i] == -1) {
            queue[i] = count;
            return;
        }
    }
}

// Looks for the given count and sends it to the back of the queue
void resetProcessCount(int count) {
    for (int i = 0; i < PROC_MAX; i++) {
        if (queue[i] == count && queue[i+1] != -1) {
            queue[i] = queue[i+1];
            queue[i+1] = count;
        }
    }
}

// Removes the given count if in queue
void removeProcessCount() {
    queue[0] = -1;
    for (int i = 0; i < PROC_MAX; i++) {
        if (queue[i] == -1) {
            queue[i] = queue[i+1];
            queue[i+1] = -1;
        }
    }
}
// returns the next count in queue
int getNextProcessCount() {
    return queue[0];
}
