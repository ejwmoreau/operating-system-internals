#include <stdio.h>
#include <unistd.h>

pid_t waitpid(pid_t pid, int *stat_loc, int options);

int main(int argc, char* argv[]) {

    if (argc < 0 && sizeof(argv) < 0) {}

    int pid = fork();

    if (pid > 0) {
        printf("Parent!\n");
        waitpid(pid, NULL, 0);
    }

    else if (pid == 0) {
        printf("Child!\n");

        freopen("/usr/share/dict/words", "r", stdin);
        freopen("words.txt", "w", stdout);

        execlp("cat", "cat", NULL);
        printf("shouldn't get here!\n");
    }

    else if (pid < 0) {
        // TODO handle errors
    }

    printf("Finished! %d\n", pid);

}
