#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char** argv) {
    char buffer[1024];

    while (1) {
        if (fgets(buffer, sizeof(buffer), stdin) != NULL) {
            char* directory = strtok(buffer, " \n");

            if (directory != NULL) {
                chdir(directory);
                // error handling
                // if (chdir(directory) < 0) {
                //     switch (errno) {
                //         ...
                //     }
                // }
            } else {
                char cwd[255];
                getcwd(cwd, sizeof(cwd));
                printf("cwd = %s\n", cwd);
            }
        } else {
            printf("Bye\n");
            exit(1);
        }
    }
}
