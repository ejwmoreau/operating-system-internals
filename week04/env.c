#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char** argv) {
    char buffer[1024];

    while (1) {
        if (fgets(buffer, sizeof(buffer), stdin) != NULL) {
            char* key = strtok(buffer, " \n=");
            char* val = strtok(NULL, " \n=");

            if (val == NULL) {
                printf("env[%s] = %s\n", key, getenv(key));
            } else {
                setenv(key, val, 1);
            }
        } else {
            printf("Bye\n");
            exit(1);
        }
    }
}
