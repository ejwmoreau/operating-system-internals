#include <stdio.h>
#include <string.h>

#define MAX 1024

extern char** environ;
int system(const char* command);
char* getcwd(char* buf, size_t size);
int chdir(const char* path);
int setenv(const char* name, const char* value, int overwrite);

void shellDir(char* location) {
    char buf[MAX];
    snprintf(buf, sizeof(buf), "ls -al %s", location);
    system(buf);
}

void shellEnviron() {
    int i = 0;
    while (environ[i] != NULL) {
        printf("%s\n", environ[i]);
        i++;
    }
}

void shellPrintCwd() {
    char cwd[255];
    getcwd(cwd, sizeof(cwd));
    printf("%s\n", cwd);
}

void shellChDir(char* argTwo) {
    chdir(argTwo);
    char cwd[255];
    getcwd(cwd, sizeof(cwd));
    setenv("PWD", cwd, 1);
}

void shellUnknown() {
    printf("Unknown arguments\n");
}

int main(int argc, char** argv) {

    while (1) {
        char cwd[255];
        getcwd(cwd, sizeof(cwd));
        printf("%s >> ", cwd);

        char buf[MAX];

        if (fgets(buf, MAX, stdin)) {
            char* argOne = strtok(buf, " \n");

            char* argTwo;

            if (argOne == NULL) {
                shellUnknown();
                continue;
            }

            argTwo = strtok(NULL, " \n");

            if (!strncmp(argOne, "environ", MAX)) {
                shellEnviron();
            } else if (!strncmp(argOne, "clr", MAX)) {
                system("clear");
            } else if (!strncmp(argOne, "quit", MAX)) {
                return 0;
            } else if (!strncmp(argOne, "cd", MAX) && argTwo == NULL) {
                shellPrintCwd();
                continue;
            }

            if (argTwo == NULL) {
                shellUnknown();
                continue;
            }

            if (!strncmp(argOne, "dir", MAX)) {
                shellDir(argTwo);
            } else if (!strncmp(argOne, "cd", MAX)) {
                shellChDir(argTwo);
            } else {
                shellUnknown();
            }
        }
    }

    return 0;
}
