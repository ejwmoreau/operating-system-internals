/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

/*
 * Runs the entire program myshell
 * - Goes through each input line (either stdin or file)
 * - Execution of commands is taken care of by other source files
 */
int main(int argc, char** argv) {
    char* cwd = getenv("PWD");

    // Set the SHELL variable in environ
    char* absolutePath = realpath(argv[0], cwd);
    setenv("SHELL", absolutePath, 1);

    // Check if a batchfile is given and open it
    FILE* input = stdin;
    if (argc > 1) {
        input = fopen(argv[1], "r");
        if (input == NULL) {
            printf("Error: File could not be opened\n");
            return 0;
        }
    }

    while (1) {
        // Prints the CWD if stdin
        if (input == stdin) {
            getcwd(cwd, strlen(cwd));
            printf("%s >> ", cwd);
        }

        char line[LINE_MAX];

        // Reads one line from stdin
        if (fgets(line, LINE_MAX, input)) {

            // Parse the given line into an array
            char* args[ARGS_MAX];
            parseLine(args, ARGS_MAX, line);

            // Print new line if no command was entered
            if (args[0] == NULL) {
                printf("\n");
                continue;
            }

            // Run the function related to that command
            if (!strcmp(args[0], "cd")) {
                cdShell(args);
            } else if (!strcmp(args[0], "clr")) {
                clrShell();
            } else if (!strcmp(args[0], "dir")) {
                dirShell(args);
            } else if (!strcmp(args[0], "environ")) {
                environShell(args);
            } else if (!strcmp(args[0], "echo")) {
                echoShell(args);
            } else if (!strcmp(args[0], "help")) {
                helpShell(args);
            } else if (!strcmp(args[0], "pause")) {
                pauseShell();
            } else if (!strcmp(args[0], "quit")) {
                return 1;
            } else {
                externShell(args);
            }
        } else {
            return 0;
        }
    }
}

int checkBackground(char** args) {
    for (int i = 0; args[i] != NULL; i++) {
        // Checks if "&" is given and returns true
        if (!strcmp(args[i], "&")) {
            args[i] = NULL;
            return 0;
        }
    }
    return 1;
}

int redirectStart(char** args) {

    // Used to NULL out the correct argument with input
    int save = -1;

    // Check if any redirection is needed
    for (int i = 0; args[i+1] != NULL; i++) {
        if (!strcmp(args[i], "<")) {
            // Adds a NULL and saves the location of the file
            args[i] = args[i+1];
            save = i+1;
        } else if (!strcmp(args[i], ">")) {
            // Redirects stdout
            if (freopen(args[i+1], "w", stdout) == NULL) {
                printf("Error: Output redirection file could not be opened\n");
                return 1;
            }
            args[i] = NULL;
        } else if (!strcmp(args[i], ">>")) {
            // Redirects stdout
            if (freopen(args[i+1], "a", stdout) == NULL) {
                printf("Error: Output redirection file could not be opened\n");
                return 1;
            }
            args[i] = NULL;
        }
    }

    // Puts the input file forward
    if (save != -1) {
        args[save] = NULL;
    }

    return 0;
}

void parseLine(char** args, int length, char* line) {
    char* temp = strtok(line, " \t\n");

    // Extract all arguments separated by spaces/tabs/newline
    int i = 0;
    for (; temp != NULL && i < length; i++) {
        args[i] = temp;
        temp = strtok(NULL, " \t\n");
    }

    args[i] = NULL;
}
