/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void printDir(char* path) {
    // Set the SHELL variable
    char* parent = getenv("SHELL");
    setenv("PARENT", parent, 1);

    // Execute command
    if (execlp("ls", "ls", "-al", path, NULL) == -1) {
        printf("Error: Exec not working in dirShell.c\n");
    }
}

void dirShell(char** args) {
    // Fork and let child execute 'ls'
    int pid = fork();

    if (pid == 0) {
        // Redirect if needed
        redirectStart(args);

        // Check through a maximum of two arguments
        if (args[1] == NULL) {
            // Set path to CWD
            getcwd(args[1], LINE_MAX);
            printDir(args[1]);
        } else {
            // Takes care of the first path
            printDir(args[1]);

            // If there is a second path given
            if (args[2] != NULL) {
                printDir(args[2]);
            }
        }
    }

    else if (pid > 0) {
        waitpid(pid, NULL, 0);
    }

    else {
        printf("Error: Fork not working in dirShell.c\n");
    }

}
