/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void helpShell(char** args) {
    // Fork and let child execute 'more'
    int pid = fork();

    if (pid == 0) {
        // Redirect if needed
        redirectStart(args);

        char* parent = getenv("SHELL");
        setenv("PARENT", parent, 1);

        if (execlp("more", "more", "readme", NULL) == -1) {
            printf("Error: Exec not working in helpShell.c\n");
        }
    }

    else if (pid > 0) {
        waitpid(pid, NULL, 0);
    }

    else {
        printf("Error: Fork not working in helpShell.c\n");
    }
}
