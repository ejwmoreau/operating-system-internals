/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

////////////////////////////////////////
// All #includes
////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <termios.h>

////////////////////////////////////////
// All #defines
////////////////////////////////////////

#define LINE_MAX 255
#define CHDIR_MAX 255
#define ARGS_MAX 255

////////////////////////////////////////
// Variables from external libraries
////////////////////////////////////////

extern char** environ;

////////////////////////////////////////
// Functions from external libraries
////////////////////////////////////////

int chdir(const char* path);
char* getcwd(char* buf, size_t size);
char* getenv(const char* name);
int setenv(const char* name, const char* value, int overwrite);
pid_t waitpid(pid_t pid, int* stat_loc, int options);

////////////////////////////////////////
// Functions implemented
////////////////////////////////////////

// myshell.c
// Checks for any need to run process in background
int checkBackground(char** args);
// Handles any redirection that needs to take place
int redirectStart(char** args);
// Parses the given line into an array and removes spaces/tabs/newline
void parseLine(char** buf, int length, char* line);

// cdShell.c
// Changes the CWD, or prints PWD if no arguments are given
void cdShell(char** args);

// clrShell.c
// Clears the terminal and puts prompt at top of screen
void clrShell();

// dirShell.c
// Handles all the directory arguments given
void dirShell(char** args);
// Prints all the contents of the given directory
void printDir(char* path);

// environShell.c
// Prints all the environment variables in environ
void environShell(char** args);

// echoShell.c
// Prints all the arguments given, or a newline if not arguments are given
void echoShell(char** args);

// helpShell.c
// Opens the readme user manual using "more"
void helpShell(char** args);

// pauseShell.c
// Pauses the shell and prevents keys being echoed until [Enter] is pressed
void pauseShell();

// externShell.c
// Handles any external commands that might be given, with the given arguments
void externShell(char** args);
