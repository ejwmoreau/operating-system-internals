/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void echoShell(char** args) {
    // Fork and let child run echo
    int pid = fork();

    if (pid == 0) {
        // Redirect if needed
        redirectStart(args);

        if (args[1] == NULL) {
            printf("\n");
            return;
        }

        // Goes through the args and prints all arguments
        int i;
        for (i = 1; args[i+1] != NULL; i++) {
            printf("%s ", args[i]);
        }

        // Prints the last argument with a new line, instead of a space
        printf("%s\n", args[i]);

        exit(0);
    }

    else if (pid > 0) {
        waitpid(pid, NULL, 0);
    }

    else {
        printf("Error: Fork not working in echoShell.c\n");
    }
}
