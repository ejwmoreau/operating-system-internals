/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void externShell(char** args) {

    int background = checkBackground(args);

    // Fork and let child execute command
    int pid = fork();

    if (pid == 0) {
        // Redirect if needed
        redirectStart(args);

        char* parent = getenv("SHELL");
        setenv("PARENT", parent, 1);

        if (execvp(args[0], args) == -1) {
            printf("Error: Unknown command\n");
        }
    }

    else if (pid > 0) {
        if (background) {
            waitpid(pid, NULL, 0);
        }
    }

    else {
        printf("Error: Fork not working in externShell.c\n");
    }
}
