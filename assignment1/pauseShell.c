/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void pauseShell() {
    printf("Press [Enter] to continue\n");

    // Retrieves the flags for the terminal
    int ttyDevice = STDOUT_FILENO;
    struct termios termiosFlags;
    tcgetattr(ttyDevice, &termiosFlags);

    // Stores the current value
    int prev = termiosFlags.c_lflag;

    // Changes the current value to prevent echoing keys
    termiosFlags.c_lflag = ECHONL;
    tcsetattr(ttyDevice, 0, &termiosFlags);

    // Waits for [Enter] to be pressed
    char buf[LINE_MAX];
    fgets(buf, LINE_MAX, stdin);

    // Reset the flag to allow echoing keys
    termiosFlags.c_lflag = prev;
    tcsetattr(ttyDevice, 0, &termiosFlags);
}
