/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void clrShell() {
    // Fork and let child execute 'clear'
    int pid = fork();

    if (pid == 0) {
        char* parent = getenv("SHELL");
        setenv("PARENT", parent, 1);

        if (execlp("clear", "clear", NULL) == -1) {
            printf("Error: Exec not working in clrShell.c\n");
        }
    }

    else if (pid > 0) {
        waitpid(pid, NULL, 0);
    }

    else {
        printf("Error: Fork not working in clrShell.c\n");
    }
}
