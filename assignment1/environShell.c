/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void environShell(char** args) {
    // Fork and let child run environ
    int pid = fork();

    if (pid == 0) {
        // Redirect if needed
        redirectStart(args);

        for (int i = 0; environ[i] != NULL; i++) {
            printf("%s\n", environ[i]);
        }

        char* parent = getenv("SHELL");
        setenv("PARENT", parent, 1);

        exit(0);
    }

    else if (pid > 0) {
        waitpid(pid, NULL, 0);
    }

    else {
        printf("Error: Fork not working in environShell.c\n");
    }
}
