/*
 * Student Name: Elie Moreau
 * Student Number: 430085543
 */

#include "myshell.h"

void cdShell(char** args) {
    // If no path is given, prints the CWD
    if (args[1] == NULL) {
        args[1] = getenv("PWD");
        printf("%s\n", args[1]);
        return;
    }

    // Changes the CWD, as well as PWD in environ
    if (chdir(args[1]) == 0) {
        char buf[CHDIR_MAX];
        getcwd(buf, CHDIR_MAX);
        setenv("PWD", buf, 1);
    } else {
        printf("Error: Invalid directory\n");
    }
}
