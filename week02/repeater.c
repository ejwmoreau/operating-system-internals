#include <stdio.h>
#include <stdlib.h>

int main() {
    char* text = NULL;

    text = (char*) malloc (50 * sizeof(char));
    while (scanf("%s", text) > 0) {
        printf("You have entered \"%s\".\n", text);
    }
    free(text);
    exit(0);
}
