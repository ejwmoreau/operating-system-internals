#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

unsigned int sleep(unsigned int);
pid_t getpid(void);

int main(int argc, char** argv) {
    
    if (argc != 2) {
        fprintf(stderr, "You have entered the wrong number of arguments!\n");
        exit(1);
    }

    int numOfArg = atoi(argv[1]);
    for (int i = 1; i <= numOfArg; i++) {
        sleep(1);
        pid_t pid = getpid();
        printf("%d %d\n", pid, i);
    }

    return 0;
}
