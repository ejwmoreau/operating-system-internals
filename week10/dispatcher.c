#include "dispatcher.h"

int queue1[PROC_MAX+1] = {-1, -1, -1, -1, -1, -1};
int queue2[PROC_MAX+1] = {-1, -1, -1, -1, -1, -1};
int queue3[PROC_MAX+1] = {-1, -1, -1, -1, -1, -1};

int main(int argc, char** argv) {

    if (argc < 2) {
        printf("Please provide a file name\n");
        return 0;
    }

    // Used to manage processes
    process* procArray[PROC_MAX];
    process* currProcess = NULL;

    // Used to read in from file
    FILE* processFile = fopen(argv[1], "r");
    char buf[BUF_MAX];
    process* pcb;

    // Initialise procArray with each given process
    for (int i = 0; fgets(buf, BUF_MAX, processFile) != NULL; i++) {
        pcb = (process*) malloc(sizeof(process));
        pcb->pid = -1;

        char* arg = strtok(buf, ", ");
        pcb->arrivalTime = atoi(arg);

        arg = strtok(NULL, ", ");
        arg = strtok(NULL, ", ");
        pcb->remainingCpuTime = atoi(arg);

        pcb->priority = 1;

        procArray[i] = pcb;
    }

    int count = 0;
    int dispatcherTime = 0;
    int procCount = 0;
    // As long as there are still processes to run
    while (count < PROC_MAX) {
        // Add any processes that just arrived (to first priority queue)
        for (int i = procCount; i < PROC_MAX; i++) {
            if (procArray[i]->arrivalTime <= dispatcherTime) {
                addProcessCount(queue1, i);
                procCount++;
            } else {
                break;
            }
        }

        // Decrement current process time if still running
        if (currProcess != NULL) {
            currProcess->remainingCpuTime -= 1;

            // Kill current process and remove from queue if no time is remaining
            if (currProcess->remainingCpuTime < 1) {
                kill(currProcess->pid, SIGINT);

                int* queue = getQueue(currProcess->priority);
                removeProcessCount(queue);

                currProcess = NULL;
            // Otherwise suspend process and place in lower queue
            } else if (checkQueues(count)) {
                kill(currProcess->pid, SIGTSTP);

                int* oldQueue = getQueue(currProcess->priority);
                currProcess->priority = decreasePriority(currProcess->priority);
                int* newQueue = getQueue(currProcess->priority);
                resetProcessCount(oldQueue, newQueue, count);

                currProcess = NULL;
            }
        }

        // If there is no longer a running process
        if (currProcess == NULL) {
            // Get next valid count and check
            count = getNextProcessCount();
            if (count < 0) {
                break;
            }

            currProcess = procArray[count];
            // Set up and run the process if it hasn't been running
            if (currProcess->pid == -1) {

                currProcess->args[0] = "./process";
                currProcess->args[1] = NULL;
                /* Used for testing to avoid badly colored text
                char cpuTime[2];
                sprintf(cpuTime, "%d", currProcess->remainingCpuTime);
                currProcess->args[0] = "sleep";
                currProcess->args[1] = cpuTime;
                currProcess->args[2] = NULL;
                */
                currProcess->pid = fork();

                if (currProcess->pid == 0) {
                    execvp(currProcess->args[0], currProcess->args);
                }
            // Else continue running the process
            } else {
                kill(currProcess->pid, SIGCONT);
            }
        }

        sleep(1);
        // Used to test which process was being run
        //printf("%d\n", currProcess->arrivalTime);

        // Increment dispatcherTime and check if running for too long
        dispatcherTime++;
        if (dispatcherTime == 21) {
            printf("Timed Out\n");
            break;
        }
    }

    // Free all process memory
    for (int i = 0; i < PROC_MAX; i++) {
        free(procArray[i]);
    }

    return 0;
}

// Adds given count to the queue if it's not already there
void addProcessCount(int* queue, int count) {
    for (int i = 0; i < PROC_MAX; i++) {
        if (queue[i] == count) {
            return;
        } else if (queue[i] == -1) {
            queue[i] = count;
            return;
        }
    }
}

// Looks for the given count and sends it to the back of the queue
void resetProcessCount(int* oldQueue, int* newQueue, int count) {
    removeProcessCount(oldQueue);
    addProcessCount(newQueue, count);
}

// Removes the first count in queue
void removeProcessCount(int* queue) {
    queue[0] = -1;
    for (int i = 0; i < PROC_MAX; i++) {
        if (queue[i] == -1) {
            queue[i] = queue[i+1];
            queue[i+1] = -1;
        }
    }
}
// returns the next count in queue
int getNextProcessCount() {
    if (queue1[0] != -1) {
        return queue1[0];
    } else if (queue2[0] != -1) {
        return queue2[0];
    } else {
        return queue3[0];
    }
}

// returns the new priority
int decreasePriority(int priority) {
    if (priority == 3) {
        return priority;
    } else {
        return priority+1;
    }
}

// returns the appropriate queue depending on the given priority
int* getQueue(int priority) {
    if (priority == 1) {
        return queue1;
    } else if (priority == 2) {
        return queue2;
    } else {
        return queue3;
    }
}

// checks queues for any other processes waiting
int checkQueues(int count) {
    for (int i = 0; i < PROC_MAX; i++) {
        if (queue1[i] != -1 && queue1[i] != count) {
            return 1;
        } else if (queue2[i] != -1 && queue2[i] != count) {
            return 1;
        } else if (queue3[i] != -1 && queue3[i] != count) {
            return 1;
        }
    }
    return 0;
}

// prints out all queues in a nice format
void printQueues() {
    printf("Queue1     Queue2     Queue3\n");
    for (int i = 0; i < PROC_MAX; i++) {
        printf("%d: %2d      %d: %2d      %d: %2d\n", i, queue1[i], i, queue2[i], i, queue3[i]);
    }
}
