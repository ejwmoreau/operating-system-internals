#include <stdio.h>
#include <string.h>

#define MAX 1024

extern char** environ;
int system(const char* command);

void shellDir(char* location) {
    char buf[MAX];
    snprintf(buf, sizeof(buf), "ls -al %s", location);
    system(buf);
}

void shellEnviron() {
    int i = 0;
    while (environ[i] != NULL) {
        printf("%s\n", environ[i]);
        i++;
    }
}

void shellUnknown() {
    printf("Unknown arguments\n");
}

int main(int argc, char** argv) {

    while (1) {
        printf(">> ");

        char buf[MAX];

        if (fgets(buf, MAX, stdin)) {
            char* tokenOne = strtok(buf, " \n");

            char* tokenTwo;

            if (tokenOne != NULL) {
                tokenTwo = strtok(NULL, " \n");

                if (!strncmp(tokenOne, "environ", MAX)) {
                    shellEnviron();
                } else if (!strncmp(tokenOne, "clr", MAX)) {
                    system("clear");
                } else if (!strncmp(tokenOne, "quit", MAX)) {
                    return 0;
                }

                else if ((tokenTwo != NULL)) {

                    if (!strncmp(tokenOne, "dir", MAX)) {
                        shellDir(tokenTwo);
                    } else {
                        shellUnknown();
                    }

                } else {
                    shellUnknown();
                }
            } else {
                shellUnknown();
            }
        }
    }

    return 0;
}
