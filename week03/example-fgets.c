#include <stdio.h>
#include <string.h>

int system(const char* command);

int main(int argc, char** argv) {
    char input_buffer[1024];

    while (1) {
        printf(">> ");

        // errors given by NULL being returned
        if (fgets(input_buffer, sizeof(input_buffer), stdin)) {
            printf("Read line: %s", input_buffer);
        } else {

            if (feof(stdin)) {
                printf("end-of-file");
                return 0;
            } else if (ferror(stdin)) {
                printf("error!\n");
                return 1;
            }
        }
    }

    // prints as a string instead of stdout
    char buf[1024];
    snprintf(buf, sizeof(buf), "Hello %d\n", 3);

    // looks for command in PATH (declared elsewhere)
    system("clear");

    return 0;
}
