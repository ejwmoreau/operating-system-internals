#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
    int pid = fork();
    int* x = (int*)malloc(sizeof(int));
    *x = 3;

    if (pid < 0) {
        // die
    }

    else if (pid == 0) {
        *x = 4;
        printf("Child %p %d\n", (void*)x, *x);
        free(x);
    }

    else if (pid > 0) {
        for (int i = 0; i < 1000000; i++);
        printf("Parent %p %d\n", (void*)x, *x);
    }

    printf("Finished! %d\n", pid);
}
