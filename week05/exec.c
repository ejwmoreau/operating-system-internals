#include <stdio.h>
#include <unistd.h>

// execl
// execlp
// execle
// execv
// execvp
// execve
// execvpe
//
// contains l = printf
// contains v = array of strings
// contains p = search on path
// contains e = environment

int main(int argc, char* argv[]) {
    int pid = fork();

    if (pid > 0) {
        printf("Parent!\n");
    }

    else if (pid == 0) {
        printf("Child!\n");
        execlp("echo", "abc", "def", NULL);
        printf("shouldn't get here!\n");
    }

    else if (pid < 0) {
        // TODO handle errors
    }

    printf("Finished! %d\n", pid);

}
